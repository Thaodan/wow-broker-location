﻿## Interface: 70300
## Title: Broker_Location
## Notes: Keeps track of your current location.
## Version: @project-version@
## Author: CodeRedLin, iceeagle
## OptionalDeps: Ace3, LibTourist-3.0, LibQTip-1.0
## SavedVariables: Broker_LocationDB
## DefaultState: enabled

## LoadManagers: AddonLoader
## X-LoadOn-Always: Delayed

#@no-lib-strip@
embeds.xml
#@end-no-lib-strip@

Libs\LibDataBroker-1.1\LibDataBroker-1.1.lua

Broker_Location.lua
